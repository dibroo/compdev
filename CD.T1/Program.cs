﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CD.T1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(StrToInt("123456\n".GetEnumerator()));
            Console.WriteLine(StrToInt("123456-\n".GetEnumerator()));
            Console.WriteLine(StrToInt("-123456\n".GetEnumerator()));

            Console.WriteLine(StrToId("aaa1111\n".GetEnumerator()));
            Console.WriteLine(StrToId("aaa111ss1\n".GetEnumerator()));
            Console.WriteLine(StrToId("asaa1111\n".GetEnumerator()));
            Console.WriteLine(StrToId("1aaa1111\n".GetEnumerator()));
            Console.WriteLine(StrToId("aaa-1111\n".GetEnumerator()));

            Console.WriteLine(StrToSignedInt("123456\n".GetEnumerator()));
            Console.WriteLine(StrToSignedInt("123456-\n".GetEnumerator()));
            Console.WriteLine(StrToSignedInt("-123456\n".GetEnumerator()));
            Console.WriteLine(StrToSignedInt("-1\n".GetEnumerator()));
            Console.WriteLine(StrToSignedInt("1\n".GetEnumerator()));
            Console.WriteLine(StrToSignedInt("+123456\n".GetEnumerator()));
            Console.WriteLine(StrToSignedInt("-\n".GetEnumerator()));

            Console.WriteLine(StrToLD("a1a2a3b4\n".GetEnumerator()));
            Console.WriteLine(StrToLD("a1a2a3a\n".GetEnumerator()));
            Console.WriteLine(StrToLD("1a2s3f\n".GetEnumerator()));
            Console.WriteLine(StrToLD("+123456\n".GetEnumerator()));
            Console.WriteLine(StrToLD("-\n".GetEnumerator()));

            Console.WriteLine(StrToIntList("1 2 3   4 5 6\n").Aggregate("", (a, x) => a + x + ","));
            Console.WriteLine(StrToIntList("1 2\n").Aggregate("", (a, x) => a + x + ","));
            Console.WriteLine(StrToIntList("\n"));
            Console.WriteLine(StrToIntList("+1234   -56\n").Aggregate("", (a, x) => a + x + ","));
            Console.WriteLine(StrToIntList("1 2 3 4 a\n"));

            Console.WriteLine(LDGroups("aa12c23dd1\n"));
            Console.WriteLine(LDGroups("aa12c\n"));
            Console.WriteLine(LDGroups("aa122c23dd1\n"));
            Console.WriteLine(LDGroups("a\n"));

            Console.WriteLine(StrToDouble("1111.111\n"));
        }

        static bool And(IEnumerator<char> chars, ref string acc, params Predicate<char>[] ps)
        {
            if (ps.All(x => x(chars.Current)))
            {
                acc += chars.Current;
                return chars.MoveNext();
            }
            return false;
        }

        static bool Or(IEnumerator<char> chars, ref string acc, params Predicate<char>[] ps)
        {
            if (ps.Any(x => x(chars.Current)))
            {
                acc += chars.Current;
                return chars.MoveNext();
            }
            return false;
        }

        static bool CanOr(IEnumerator<char> chars, ref string acc, params Predicate<char>[] ps)
        {
            if (ps.Any(x => x(chars.Current)))
            {
                acc += chars.Current;
                return chars.MoveNext();
            }
            return true;
        }

        static bool Can(IEnumerator<char> chars, ref string acc, params Predicate<char>[] ps)
        {
            if (ps.Any(x => x(chars.Current)))
            {
                acc += chars.Current;
                return chars.MoveNext();
            }
            return true;
        }

        static bool Any(IEnumerator<char> chars, ref string acc, params Predicate<char>[] ps)
        {
            while (ps.Any(x => x(chars.Current)))
            {
                acc += chars.Current;
                if (!chars.MoveNext()) return false;
            }
            return true;
        }

        static bool OneAndMore(IEnumerator<char> chars, ref string acc, params Predicate<char>[] ps)
        {
            if (ps.Any(x => x(chars.Current)))
            {
                acc += chars.Current;
                if (!chars.MoveNext())
                    return false;
            }
            else
                return false;

            while(ps.Any(x => x(chars.Current)))
            {
                acc += chars.Current;
                if (!chars.MoveNext())
                    return false;
            }
            return true;
        }

        static bool End(IEnumerator<char> chars)
            => chars.Current == '\n';
        

        static int? StrToInt(IEnumerator<char> c)
        {
            var s = "";

            if (!c.MoveNext()) return null;
            if (!OneAndMore(c, ref s, char.IsDigit)) return null;
            if (!End(c)) return null;

            return int.Parse(s);
        }

        static string StrToId(IEnumerator<char> c)
        {
            var s = "";
            if (!c.MoveNext()) return null;
            if (!char.IsLetter(c.Current)) return null;

            s += c.Current;

            while(c.MoveNext())
            {
                if (char.IsLetter(c.Current) || char.IsDigit(c.Current))
                    s += c.Current;
                else if (c.Current == '\n')
                    return s;
                else return null;
            }

            return s;
        }

        static int? StrToSignedInt(IEnumerator<char> c)
        {
            var s = "";

            if (!c.MoveNext()) return null;
            if (!CanOr(c, ref s, (x => x == '-'), x => x == '+')) return null;
            if (!OneAndMore(c, ref s, char.IsDigit)) return null;
            if (!End(c)) return null;

            return int.Parse(s);
        }

        static string StrToLD(IEnumerator<char> c)
        {
            var s = "";

            if (!c.MoveNext()) return null;
            if (!And(c, ref s, char.IsLetter)) return null;
            while (And(c, ref s, char.IsDigit) && And(c, ref s, char.IsLetter)) ;
            if (!Can(c, ref s, char.IsDigit)) return null;
            if (!End(c)) return null;
            return s;
        }

        static LinkedList<int> StrToIntList(IEnumerable<char> en)
        {
            var c = en.GetEnumerator();
            var res = new LinkedList<int>();
            if (!c.MoveNext()) return null;
            
            while(true)
            {
                var s = "";

                if (!CanOr(c, ref s, (x => x == '-'), x => x == '+')) return null;
                if (!OneAndMore(c, ref s, char.IsDigit)) return null;

                var i = int.Parse(s);
                res.AddLast(i);

                if (End(c))
                    return res;
                var ws = "";
                if (!OneAndMore(c, ref ws, char.IsWhiteSpace))
                    return null;
            }
        }

        static string LDGroups(IEnumerable<char> en)
        {
            var c = en.GetEnumerator();
            var s = "";

            if (!c.MoveNext()) return null;
            if (!And(c, ref s, char.IsLetter)) return null;
            if (!Can(c, ref s, char.IsLetter)) return null;
            while (
                And(c, ref s, char.IsDigit) &&
                Can(c, ref s, char.IsDigit) &&
                And(c, ref s, char.IsLetter) &&
                Can(c, ref s, char.IsLetter)
                ) ;
            if (!Can(c, ref s, char.IsDigit)) return null;
            if (!Can(c, ref s, char.IsDigit)) return null;
            if (!End(c)) return null;
            return s;
        }

        static double? StrToDouble(IEnumerable<char> en)
        {
            var c = en.GetEnumerator();
            var s = "";

            if (!c.MoveNext()) return null;
            if (!OneAndMore(c, ref s, char.IsDigit)) return null;
            if (!And(c, ref s, x => x == '.')) return null;
            if (!OneAndMore(c, ref s, char.IsDigit)) return null;
            if (!End(c)) return null;

            return double.Parse(s.Replace('.', ','));
        }

        static string StrToStr(IEnumerable<char> en)
        {
            var c = en.GetEnumerator();
            var s = "";

            if (!c.MoveNext()) return null;
            if (!And(c, ref s, x => x == '\'')) return null;
            if (!OneAndMore(c, ref s, x => x != '\'')) return null;
            if (!And(c, ref s, x => x == '\'')) return null;
            if (!End(c)) return null;

            return s;
        }
    }
}
